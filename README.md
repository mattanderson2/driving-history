# Driving History Solution 
### Matthew Anderson

#
To implement my solution to the driving history problem, I chose to represent the Driver and
Trip as classes. This seemed to be the easiest implementation for this project, since the 
problem statement seems to be built around the idea that trip and driver have their own attributes
and actions that can be completed for each. It also makes it easy to implement unit tests,
since the core functionality of the application can be tested simply by creating instances
of classes and testing each of the methods. Tests are split up for each of the classes 
within the "test" directory. 

In addition to using classes, I stored most of these data objects in lists. ArrayLists for
both Driver and Trip exist, to store each instance of the class created to be accessed at
later points. While this was a very straightforward way to store the data objects, I must 
concede that it is not the most efficient. At a certain point in the program's execution,
iteration through both lists occurs, resulting in a time complexity of O(n^2).
A better solution might have been to introduce a HashMap of Trips, but the key/value pair of 
Driver/Trip would not actually improve runtime.

I made it possible for the user to enter the file to be read in the command-line, 
but if no argument is provided, the default text file used is "input.txt". My 
implementation assumes that all input provided is correct upon running, e.g. times are 
always provided when necessary, times are formatted correctly, no negative numbers are entered for the miles driven, etc. The program does 
check the command that is entered, and skips over it if is not "Driver" or "Trip". 

To sort the Drivers based on most miles driven, the compareTo method is overridden in the Driver
class, which implements the Comparable interface. 