import org.junit.Assert;
import org.junit.Test;
import java.text.ParseException;
import static org.junit.Assert.*;

public class DriverTest {

    @Test
    public void testGetName_ShouldBeEqual() {

        // Arrange
        Driver driver = new Driver("Max");

        // Act
        String expected = "Max";
        String actual = driver.getName();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetName_ShouldBeNotEqual() {

        // Arrange
        Driver driver = new Driver("Max");

        // Act
        String expected = "Mex";
        String actual = driver.getName();

        // Assert
        assertNotEquals(expected, actual);
    }

    @Test
    public void setTripList_TripIsNotDiscarded_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver = new Driver("Ella");
        driver.setTripList(new Trip("19:25", "20:25", 64));
        driver.setTripList(new Trip("21:20", "23:15", 100));

        // Act
        int expected = 2;
        int actual = driver.getTripList().size();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void setTripList_AverageSpeedLessThan5_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver = new Driver("Max");
        driver.setTripList(new Trip("03:00", "04:00", 1)); // trip should not be added

        // Act
        int expected = 0;
        int actual = driver.getTripList().size(); // size of the trip list should be zero

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void setTripList_AverageSpeedGreaterThan100_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver = new Driver("Max");
        driver.setTripList(new Trip("03:00", "04:00", 500)); // trip should not be added

        // Act
        int expected = 0;
        int actual = driver.getTripList().size(); // size of the trip list should be zero

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetTotalMiles_TotalMilesRoundsDown_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver = new Driver("Max");
        driver.setTripList(new Trip("06:12", "06:32", 14.3));
        driver.setTripList(new Trip("13:12", "13:52", 25.97));
        driver.setTotalMiles();

        // Act
        int expected = 40;
        int actual = driver.getTotalMiles(); // 14.3 + 25.97 = 40.27, should round down to 40

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetTotalMiles_TotalMilesRoundsUp_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver = new Driver("Max");
        driver.setTripList(new Trip("06:12", "06:32", 10.5));
        driver.setTripList(new Trip("13:12", "13:52", 10));
        driver.setTotalMiles();

        // Act
        int expected = 21;
        int actual = driver.getTotalMiles(); // 10.5 + 10 should round up to 21

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetTotalMiles_TotalMilesIsZero_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver = new Driver("Max");
        driver.setTripList(new Trip("06:12", "06:32", 0));
        driver.setTripList(new Trip("13:15", "20:15", 0));
        driver.setTotalMiles();

        // Act
        int expected = 0;
        int actual = driver.getTotalMiles();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetTotalMiles_NoTripsAdded_ShouldBeEqual() {

        // Arrange
        Driver driver = new Driver("Max");
        driver.setTotalMiles();

        // Act
        int expected = 0;
        int actual = driver.getTotalMiles();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetTotalAverageSpeed_TotalSpeedRoundsUp_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver = new Driver("Max");
        driver.setTripList(new Trip("06:12", "06:32", 10.5));
        driver.setTripList(new Trip("13:12", "13:52", 30));
        driver.setTotalMiles();
        driver.setTotalAverageSpeed(); // total average speed = 40.5

        // Act
        int expected = 41;
        int actual = driver.getTotalAverageSpeed();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetTotalAverageSpeed_TotalSpeedRoundsDown_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver = new Driver("Max");
        driver.setTripList(new Trip("06:12", "06:32", 14.3));
        driver.setTripList(new Trip("13:12", "13:52", 25.97));
        driver.setTotalMiles();
        driver.setTotalAverageSpeed();

        // Act
        int expected = 40;
        int actual = driver.getTotalAverageSpeed(); // total average Speed = 40.27

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCompareTo_GreaterThan_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver1 = new Driver("John");
        driver1.setTripList(new Trip("06:12", "06:32", 10));
        driver1.setTripList(new Trip("06:12", "06:32", 15));
        Driver driver2 = new Driver("Michael");
        driver2.setTripList(new Trip("06:12", "06:32", 10));
        driver1.setTotalMiles();
        driver2.setTotalMiles();

        // Act
        int expected = -1;
        int actual = driver1.compareTo(driver2);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCompareTo_LessThan_ShouldBeEqual() throws ParseException {

        // Arrange
        Driver driver1 = new Driver("John");
        driver1.setTripList(new Trip("06:12", "06:32", 5));
        Driver driver2 = new Driver("Michael");
        driver2.setTripList(new Trip("06:12", "06:32", 10));
        driver1.setTotalMiles();
        driver2.setTotalMiles();

        // Act
        int expected = 1;
        int actual = driver1.compareTo(driver2);

        // Assert
        assertEquals(expected, actual);
    }
}