import org.junit.Assert;
import org.junit.Test;
import java.text.ParseException;
import static org.junit.Assert.*;

public class TripTest {
    private static final double DELTA = 1e-15;

    @Test
    public void testGetMiles_ShouldBeEqual() throws ParseException {

        // Arrange
        Trip trip = new Trip("3:15", "7:20", 44.23);

        // Act
        double expected = 44.23;
        double actual = trip.getMiles();

        // Assert
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetMiles_ShouldBeNotEqual() throws ParseException {

        // Arrange
        Trip trip = new Trip("3:15", "7:20", 1.023);

        // Act
        double expected = 1.02300001;
        double actual = trip.getMiles();

        // Assert
        assertNotEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetTotalTripTime_HourDifference_ShouldBeEqual() throws ParseException {

        // Arrange
        Trip trip = new Trip("3:15", "4:15", 25);

        // Act
        double expected = 1;
        double actual = trip.getTotalTripTime();

        // Assert
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetTotalTripTime_MinuteDifference_ShouldBeEqual() throws ParseException {

        // Arrange
        Trip trip = new Trip("19:27", "19:42", 25);

        // Act
        double expected = 0.25;
        double actual = trip.getTotalTripTime();

        // Assert
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetTotalTripTime_HourAndMinuteDifference_ShouldBeEqual() throws ParseException {

        // Arrange
        Trip trip = new Trip("3:15", "10:30", 25);

        // Act
        double expected = 7.25;
        double actual = trip.getTotalTripTime();

        // Assert
        assertEquals(expected, actual, DELTA);
    }

    @Test
    public void testGetTotalTripTime_SameTimes_ShouldBeEqual() throws ParseException {

        // Arrange
        Trip trip = new Trip("10:00", "10:00", 0);

        // Act
        double expected = 0;
        double actual = trip.getTotalTripTime();

        // Assert
        assertEquals(expected, actual, DELTA);
    }
}