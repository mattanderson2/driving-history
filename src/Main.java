import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Entry point for the program. Reads through a text file and creates instances of each Driver and Trip.
 * Outputs the total miles driven for each driver and the average speed across each of their trips.
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException, ParseException {

        // Basic setup
        File file;
        ArrayList<Driver> driverList = new ArrayList();
        Driver driver;
        if (args.length > 0) {
            file = new File(args[0]);
        } else {
            file = new File("input.txt");
        }
        Scanner scan = new Scanner(file);

        // Read through text file and create instances of each Driver and Trip
        while(scan.hasNext()) {

            String command = scan.next();
            if (command.equals("Driver")) {
                driver = new Driver(scan.next());
                driverList.add(driver);
            }
            else if (command.equals("Trip")) {
                String driverName = scan.next();
                for (Driver existingDriver : driverList) {

                    if (existingDriver.getName().equals(driverName)) {
                        Trip trip = new Trip(scan.next(), scan.next(), scan.nextDouble());
                        existingDriver.setTripList(trip);
                    }
                }
            } else {
                System.out.println("Invalid command: " + command);
            }
        }

        // Find total miles and total average speed, then sort the list of drivers
        for (Driver existingDriver : driverList) {
            existingDriver.setTotalMiles();
            existingDriver.setTotalAverageSpeed();
        }
        Collections.sort(driverList);

        // Output results
        for (Driver existingDriver : driverList) {
            String result = String.format("%s: %d miles", existingDriver.getName(), existingDriver.getTotalMiles());
            if (existingDriver.getTotalMiles() > 0) {
                result = result.concat(String.format(" @ %d mph", existingDriver.getTotalAverageSpeed()));
            }
            System.out.println(result);
        }
        scan.close();
    }
}