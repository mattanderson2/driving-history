import java.util.ArrayList;

/**
 * Represents each Driver. Includes data members for the driver's name, the list of trips associated with that driver
 * the total miles of all the trips taken, and the total average speed.
 */
public class Driver implements Comparable<Driver> {

    private final String name;
    private final ArrayList<Trip> tripList = new ArrayList();
    private double totalMiles = 0;
    private double totalAverageSpeed = 0;

    public Driver(String name) {
        this.name = name;
    }

    // return Driver's name
    public String getName() { return name; }

    // add a trip to the Driver's list of trips, if the trip's average speed is greater then 5 mph and less than 100 mph
    public void setTripList(Trip trip) {
        double averageSpeed = trip.getMiles() / trip.getTotalTripTime();
        if(!(averageSpeed < 5) && !(averageSpeed > 100)) {
            tripList.add(trip);
        }
    }

    // return list of trips (used only for testing purposes)
    public ArrayList<Trip> getTripList() { return tripList; }

    // sum of the miles of all trips
    public void setTotalMiles() {
        for (Trip trip: tripList) {
            totalMiles += trip.getMiles();
        }

    }

    // return total miles of all trips rounded to a whole number
    public int getTotalMiles() { return (int)Math.round(totalMiles); }

    // sets the total average speed across all trips the driver has taken
    public void setTotalAverageSpeed() {
        double totalTripTimeSum = 0;
        for (Trip trip: tripList) {
            totalTripTimeSum += trip.getTotalTripTime();
        }
        try {
            totalAverageSpeed = totalMiles / totalTripTimeSum;
        } catch (ArithmeticException e) {
            System.out.println(e);
        }
    }

    // return the total average speed of all trips
    public int getTotalAverageSpeed() { return (int)Math.round(totalAverageSpeed); }

    // override the compareTo method to sort the list of drivers based on the total number of miles
    // driven from greatest to least
    @Override
    public int compareTo(Driver driver) {
        if (this.getTotalMiles() > driver.getTotalMiles())
            return -1;
        else
            return 1;
    }
}
