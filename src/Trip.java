import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents each Trip. Includes data members for start time, end time and miles.
 * The SimpleDateFormat variable is used to convert the dates represented as strings into times that
 * will be be used to calculate the total time of the trip.
 */
public class Trip  {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
    private Date startTime;
    private Date endTime;
    private double miles;

    public Trip(String startTime, String endTime, double miles) throws ParseException {
        this.startTime = simpleDateFormat.parse(startTime);
        this.endTime = simpleDateFormat.parse(endTime);
        this.miles = miles;
    }

    // Return miles for the trip
    public double getMiles() { return miles; }

    // Return the time of the trip in hours: 1 hour returns 1, 30 minutes returns 0.5
    public double getTotalTripTime() {
        double hourDifference = ((endTime.getTime() - startTime.getTime()) / (60 * 60 * 1000)) % 24;
        double minuteDifference = ((endTime.getTime() - startTime.getTime()) / (60 * 1000)) % 60;
        return hourDifference + (minuteDifference / 60);
    }
}
